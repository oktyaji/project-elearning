@extends('layouts.app')

<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> App landing</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("admin/assets/img/favicon.ico") }}">

	<!-- CSS here -->
	<link rel="stylesheet" href="{{ asset("admin/assets/css/bootstrap.min.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/owl.carousel.min.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/slicknav.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/flaticon.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/progressbar_barfiller.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/gijgo.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/animate.min.css") }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/css/animated-headline.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/magnific-popup.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/fontawesome-all.min.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/themify-icons.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/slick.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/nice-select.css") }}">
	<link rel="stylesheet" href="{{ asset("admin/assets/css/style.css") }}">
</head>
<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{ asset("admin/assets/img/logo/loder.png") }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start-->


    <main class="login-body" data-vide-bg="{{ asset("admin/assets/img/login-bg.mp4") }}">
        <!-- Login Admin -->
        <form class="form-default" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="login-form">
                <!-- logo-login -->
                <div class="logo-login">
                    <a href="/"><img src="{{ asset("admin/assets/img/logo/loder.png") }}" alt=""></a>
                </div>
                <h2>Login Here</h2>
                <div class="form-input">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-input">
                    <label for="password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-input pt-30">
                    <input type="submit" name="submit" value="login">
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}" class="forget">Forget Password</a>
                    @endif

                </div>

                <!-- Forget Password -->

                <!-- Forget Password -->
            </div>
        </form>
        <!-- /end login form -->
    </main>


    <script src="{{ asset("admin/assets/js/vendor/modernizr-3.5.0.min.js") }}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{ asset("admin/assets/js/vendor/jquery-1.12.4.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/popper.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/bootstrap.min.js") }}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{ asset("admin/assets/js/jquery.slicknav.min.js") }}"></script>

    <!-- Video bg -->
    <script src="{{ asset("admin/assets/js/jquery.vide.js") }}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{ asset("admin/assets/js/owl.carousel.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/slick.min.js") }}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{ asset("admin/assets/js/wow.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/animated.headline.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.magnific-popup.js") }}"></script>

    <!-- Date Picker -->
    <script src="{{ asset("admin/assets/js/gijgo.min.js") }}"></script>
    <!-- Nice-select, sticky -->
    <script src="{{ asset("admin/assets/js/jquery.nice-select.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.sticky.js") }}"></script>
    <!-- Progress -->
    <script src="{{ asset("admin/assets/js/jquery.barfiller.js") }}"></script>

    <!-- counter , waypoint,Hover Direction -->
    <script src="{{ asset("admin/assets/js/jquery.counterup.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/waypoints.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.countdown.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/hover-direction-snake.min.js") }}"></script>

    <!-- contact js -->
    <script src="{{ asset("admin/assets/js/contact.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.form.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.validate.min.js") }}"></script>
    <script src="{{ asset("admin/assets/js/mail-script.js") }}"></script>
    <script src="{{ asset("admin/assets/js/jquery.ajaxchimp.min.js") }}"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{ asset("admin/assets/js/plugins.js") }}"></script>
    <script src="{{ asset("admin/assets/js/main.js") }}"></script>

    </body>
</html>
