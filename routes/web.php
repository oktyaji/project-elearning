<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('halaman.index');
});
Route::resource('admin', 'UserController');
Route::resource('mata-kuliah', 'UserController');
Route::resource('materi', 'UserController');
Route::resource('tugas', 'UserController');
Route::resource('hasil', 'UserController');
Route::resource('dosen', 'UserController');
Route::resource('mahasiswa', 'UserController');

// Route::middleware('auth')->group(function () {

// });
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//CRUD mahasiswa
Route::resource('user', 'MahasiswaController');
