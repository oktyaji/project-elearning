<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriModel extends Model
{
    protected $table = 'materi';

    protected $fillable = [
        'link_video', 'modul', 'mata_kuliah_id'
    ];
}
