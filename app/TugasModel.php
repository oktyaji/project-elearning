<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TugasModel extends Model
{
    protected $table = 'tugas';

    protected $fillable = [
        'tugas' ,'link_pengumpulan', 'mata_kuliah_id'
    ];
}
