<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenModel extends Model
{
    protected $table = 'dosen';

    protected $fillable = [
        'mata_kuliah_id', 'nidn', 'keterangan', 'user_id'
    ];
}
