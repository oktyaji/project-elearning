<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilModel extends Model
{
    protected $table = 'hasil';

    protected $fillable = [
        'mata_kuliah_id', 'mahasiswa_id', 'nilai', 'keterangan'
    ];
}
